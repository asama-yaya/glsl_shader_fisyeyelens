#version 330
// shadertype=<glsl>
// vert

// 渡す変数
out vec4 position;
out vec3 normal;
out vec4 texcoord;
out float sita;

// 受け取る行列
uniform mat4 matPC;
uniform mat4 matM;
uniform mat3 matNormal;

// おまじないに近いもの
layout (location = 0) in vec4 pv; 
layout (location = 2) in vec3 nv; 
layout (location = 8) in vec4 tv;

// fisheye parameter
vec2 Sift = vec2(0, -40);
float RR = 275.0;
float sitamax = 90.0f / 180.0f * 3.1415f;

void main(void){

	position = matM * pv;
	normal = normalize(matNormal * nv);
	texcoord = tv;

	// 魚眼
	vec4 tpos = position;
	float tsita = -tpos.z / sqrt(pow(tpos.x, 2) + pow(tpos.y, 2) + pow(tpos.z, 2));
	sita = acos(tsita);

	float r = (sin(sita/2.0) / sin(sitamax/2.0)) * RR;
	gl_Position.x = (tpos.x /  sqrt(pow(tpos.x, 2) + pow(tpos.y, 2))) * r + Sift.x;
	gl_Position.y = (tpos.y /  sqrt(pow(tpos.x, 2) + pow(tpos.y, 2))) * r + Sift.y;
	
	gl_Position.x = (gl_Position.x / 320.0);	// 解像度に合わせて正規化
	gl_Position.y = (gl_Position.y / 360.0);
	gl_Position.z = (-position.z-100.0)/1000.0;	// 自分より後ろにあるものも描画するようにしている。

	//if(abs(sita) >= sitamax){
	//	gl_Position.z = 3.0;
	//}

	gl_Position.w = 1.0;
	
}