#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader ------------------------------------------------------------------------------------
#pragma once
#include "AsaVertex.h"
#include "AsaTrackball.h"
#include "OBJLoader.h"

//**********************************************************************************************
// variable
//**********************************************************************************************
int window_width = 640;
int window_height = 720;

TTrackball Trackball;
OBJMESH Objmesh;

// flag
bool capture_flag = false;

// shader
GLuint glProgram;
GLuint glTexId = -1;

GLuint ofsProgram;
static GLuint fb_ofs;           // フレームバッファオブジェクト
static GLuint cb_ofs;           // カラーバッファ用のテクスチャ
static GLuint rb_ofs;           // デプスバッファ用のレンダーバッファ
static const GLenum bufs_ofs[] = {
	GL_COLOR_ATTACHMENT0_EXT, //   カラーバッファ (拡散反射光)
};


//**********************************************************************************************
// Method
//**********************************************************************************************
// shader
void InitShader();

// shader 初期化関連
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag = false);
void SetTexture(GLuint &_texID, const int _w, const int _h, const bool _rectFlag = false);
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName);
int ReadShaderSource(GLuint _shader, const char *_file);
void PrintShaderInfoLog(GLuint _shader);
void PrintProgramInfoLog(GLuint _program);

// glの関数
void display(void);
void resize(int _w, int _h);
void mouse(int _button, int _state, int _x, int _y);
void motion(int _x, int _y);
void keyboard(unsigned char _key, int _x, int _y);
void idle(void);