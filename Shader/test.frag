#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2D tex1;

// もらう変数
in vec4 position;
in vec3 normal;
in vec4 texcoord;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

void main(void){

	// テクスチャ
	vec4 color1 = texture2DProj(tex1, texcoord);

	// light
	vec3 lightPosition = vec3(100.0, 100.0, 500.0);
	vec3 light = normalize(lightPosition - position.xyz);

	// diffuse
	vec3 fnormal = normalize(normal);
	float diffuse = max(dot(light, fnormal), 0.0);
  
	// specular
	vec3 view = -normalize(position.xyz);
	vec3 halfway = normalize(light + view);
	float specular = pow(max(dot(fnormal, halfway), 0.0), 20.0);

	// 出力
	fc1 = color1 * max(diffuse, 0.5) + vec4(1.0, 1.0, 1.0, 1.0) * specular;
}
